/*
MySQL Database Backup Tools
Server:127.0.0.1:3306
Database:webman
Data:2022-07-22 17:20:47
*/
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(50) DEFAULT '' COMMENT '账号',
  `password` char(50) DEFAULT '' COMMENT '密码',
  `salt` char(50) DEFAULT '' COMMENT '盐',
  `token` char(200) DEFAULT '',
  `nickname` char(50) DEFAULT '' COMMENT '昵称',
  `roleId` int(11) DEFAULT '0' COMMENT '组id',
  `addtime` int(11) DEFAULT '0' COMMENT '创建时间',
  `sorttime` int(11) DEFAULT '0' COMMENT '最后一次登录时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，2锁定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='后台用户';
-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` (`id`,`username`,`password`,`salt`,`token`,`nickname`,`roleId`,`addtime`,`sorttime`,`status`) VALUES ('1','admin','797197fc3e6045e79bf19f48fb446e6e','qn2shvlcz14kax03ydrg5u','589ec29d94f1ffab4d3cac0b717c862b','管理员','1','1461127869','1658475596','1');
INSERT INTO `t_admin` (`id`,`username`,`password`,`salt`,`token`,`nickname`,`roleId`,`addtime`,`sorttime`,`status`) VALUES ('7','system','c797b030d249c7bc04610691e14f7b54','skowj2g3flxbed7t9zu45h','','系统','2','1461227869','0','1');

-- ----------------------------
-- Table structure for t_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_menu`;
CREATE TABLE `t_admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topid` int(11) DEFAULT '0' COMMENT '父级菜单id',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(500) DEFAULT NULL COMMENT '菜单链接地址',
  `act` text COMMENT '操作',
  `iconfont` varchar(255) DEFAULT NULL COMMENT '图标',
  `sort` int(11) DEFAULT '50' COMMENT '排序',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态：1开启，2关闭',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='后台菜单';
-- ----------------------------
-- Records of t_admin_menu
-- ----------------------------
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('1','0','首页','','','#xe622;','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('2','0','菜单','','','#xe636;','2','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('3','0','内容','','','#xe659;','3','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('4','0','模块','','','#xe634;','4','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('5','0','会员','','','#xe608;','5','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('6','0','系统','','','#xe646;','6','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('7','1','管理首页','home/main','{"1":{"name":"\u9996\u9875","act":"home_main"}}','#xe610;','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('8','1','后台菜单','menu/index','{"1":{"name":"\u5217\u8868","act":"menu_index"},"2":{"name":"\u6dfb\u52a0","act":"menu_info"},"3":{"name":"\u7f16\u8f91","act":"menu_info_id"},"5":{"name":"\u5220\u9664","act":"menu_del"}}','#xe689;','2','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('9','2','导航管理','nav/index','{"1":{"name":"\u5217\u8868","act":"nav_index"},"2":{"name":"\u6dfb\u52a0","act":"nav_info"},"3":{"name":"\u7f16\u8f91","act":"nav_info_id"},"4":{"name":"\u5220\u9664","act":"nav_del"}}','#xe635;','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('10','3','列表管理','article/index','{"1":{"name":"\u5217\u8868","act":"article_index"},"2":{"name":"\u6dfb\u52a0","act":"article_info"},"3":{"name":"\u7f16\u8f91","act":"article_info_id"},"4":{"name":"\u5220\u9664","act":"article_del"}}','','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('11','3','单页管理','page/index','{"1":{"name":"\u5217\u8868","act":"page_index"},"2":{"name":"\u6dfb\u52a0","act":"page_info"},"3":{"name":"\u7f16\u8f91","act":"page_info_id"},"4":{"name":"\u5220\u9664","act":"page_del"}}','','2','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('12','4','广告管理','banner/index','{"1":{"name":"\u5217\u8868","act":"banner_index"},"2":{"name":"\u6dfb\u52a0","act":"banner_info"},"3":{"name":"\u7f16\u8f91","act":"banner_info_id"},"4":{"name":"\u5220\u9664","act":"banner_del"}}','','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('13','4','友情链接','link/index','{"1":{"name":"\u5217\u8868","act":"link_index"},"2":{"name":"\u6dfb\u52a0","act":"link_info"},"3":{"name":"\u7f16\u8f91","act":"link_info_id"},"4":{"name":"\u5220\u9664","act":"link_del"}}','','2','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('14','5','会员管理','user/index','{"1":{"name":"\u5217\u8868","act":"user_index"},"2":{"name":"\u6dfb\u52a0","act":"user_info"},"3":{"name":"\u7f16\u8f91","act":"user_info_id"},"4":{"name":"\u5220\u9664","act":"user_del"},"5":{"name":"\u5bc6","act":"user_password"}}','','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('15','5','会员日志','userlog/index','{"1":{"name":"\u5217\u8868","act":"userlog_index"},"2":{"name":"\u5220\u9664","act":"userlog_del"}}','','2','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('16','6','系统设置','set/site','{"1":{"name":"\u7ad9\u70b9\u8bbe\u7f6e","act":"set_site"},"2":{"name":"\u6a21\u677f\u8bbe\u7f6e","act":"set_tpl"},"3":{"name":"\u624b\u673a\u8bbe\u7f6e","act":"set_mobile"},"4":{"name":"\u4e0a\u4f20\u8bbe\u7f6e","act":"set_upload"},"5":{"name":"\u77ed\u4fe1\u8bbe\u7f6e","act":"set_sms"}}','','1','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('17','6','后台用户','admin/index','{"1":{"name":"\u5217\u8868","act":"admin_index"},"2":{"name":"\u6dfb\u52a0","act":"admin_info"},"3":{"name":"\u7f16\u8f91","act":"admin_info_id"},"4":{"name":"\u5220\u9664","act":"admin_del"},"5":{"name":"\u5bc6","act":"admin_password"}}','','2','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('18','6','角色权限','role/index','{"1":{"name":"\u5217\u8868","act":"role_indel"},"2":{"name":"\u6dfb\u52a0","act":"role_info"},"3":{"name":"\u7f16\u8f91","act":"role_info_id"},"4":{"name":"\u5220\u9664","act":"role_del"},"5":{"name":"\u6743\u9650","act":"role_auth"}}','','3','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('19','6','日志记录','log/index','{"1":{"name":"\u5217\u8868","act":"log_index"}}','','4','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('20','6','前台引导','guide/index','{"1":{"name":"\u5217\u8868","act":"guide_index"},"2":{"name":"\u6dfb\u52a0","act":"guide_info"},"3":{"name":"\u7f16\u8f91","act":"guide_info_id"},"4":{"name":"\u5220\u9664","act":"guide_del"}}','','5','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('21','6','数据备份','backup/index','{"1":{"name":"\u5217\u8868","act":"backup_index"},"2":{"name":"\u5907\u4efd","act":"backup_back"},"3":{"name":"\u4e0b\u8f7d","act":"backup_download"},"4":{"name":"\u5220\u9664","act":"backup_del"},"5":{"name":"\u6570\u636e\u8868","act":"backup_mysql"}}','','6','1');
INSERT INTO `t_admin_menu` (`id`,`topid`,`name`,`url`,`act`,`iconfont`,`sort`,`status`) VALUES ('22','6','来访记录','visit/index','','','7','1');

-- ----------------------------
-- Table structure for t_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_role`;
CREATE TABLE `t_admin_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT '' COMMENT '组名称',
  `menu_purview` text COMMENT '栏目权限',
  `base_purview` text COMMENT '操作权限',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，2禁用',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='角色权限';
-- ----------------------------
-- Records of t_admin_role
-- ----------------------------
INSERT INTO `t_admin_role` (`id`,`name`,`menu_purview`,`base_purview`,`status`) VALUES ('1','超级管理员','','','1');
INSERT INTO `t_admin_role` (`id`,`name`,`menu_purview`,`base_purview`,`status`) VALUES ('2','主管','1,7,2,9,3,10,11,4,12,13,5,14,15','home_main,nav_index,nav_info,nav_info_id,nav_del,article_index,article_info,article_info_id,article_del,page_index,page_info,page_info_id,page_del,banner_index,banner_info,banner_info_id,banner_del,link_index,link_info,link_info_id,link_del,user_index,user_info,user_info_id,user_del,userlog_index,userlog_del','1');
INSERT INTO `t_admin_role` (`id`,`name`,`menu_purview`,`base_purview`,`status`) VALUES ('3','客服','1,7,3,10,11,4,12,13,5,14,15','home_main,article_index,article_info,article_info_id,article_del,page_index,page_info,page_info_id,page_del,banner_index,banner_info,banner_info_id,banner_del,link_index,link_info,link_info_id,link_del,user_index,user_info,user_info_id,user_del,userlog_index,userlog_del','1');
INSERT INTO `t_admin_role` (`id`,`name`,`menu_purview`,`base_purview`,`status`) VALUES ('4','来宾','','','1');

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_id` int(11) DEFAULT '0' COMMENT '所属栏目id',
  `title` char(50) DEFAULT '' COMMENT '文章标题',
  `thumb` char(200) DEFAULT '' COMMENT '缩略图url',
  `video` char(200) DEFAULT '' COMMENT '视频url',
  `keywords` char(100) DEFAULT '' COMMENT '关键词',
  `description` char(200) DEFAULT '' COMMENT '关键词描述',
  `subject` char(255) DEFAULT '' COMMENT '摘要',
  `label` char(50) DEFAULT '' COMMENT '标签。多个|分隔',
  `view` int(11) DEFAULT '0' COMMENT '浏览次数',
  `ishot` tinyint(1) DEFAULT '2' COMMENT '是否热门:1是,2否',
  `introduce` tinyint(1) DEFAULT '2' COMMENT '是否推荐:1是,2否',
  `tel` char(50) DEFAULT '' COMMENT '联系电话',
  `address` char(50) DEFAULT '' COMMENT '联系地址',
  `pick_time` char(50) DEFAULT '' COMMENT '采摘时间',
  `addtime` int(11) DEFAULT '0' COMMENT '首次发布的时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态；1正常，2禁用',
  PRIMARY KEY (`id`),
  KEY `navid` (`nav_id`),
  KEY `status` (`nav_id`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='文章';
-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('1','9','渝北葡萄镇采摘攻略','http://webman.tinphp.com/uploads/2022/07/1656917260M7FYr.jpg','','石船','','巨峰葡萄为每斤10元，玫瑰香葡萄为每斤15元。','葡萄采摘','0','1','2','13983834853','重庆市渝北区石船镇重桥村葡萄基地。','2022-07 - 2022-08','1656920501','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('6','9','向葡萄庄园','http://webman.tinphp.com/uploads/2022/07/1656920343xSoDU.jpg','','西彭','','30-60元/斤，品种不同价格不同','葡萄','0','2','1','','重庆市九龙坡区西彭镇真武宫村向葡萄庄园','2022-07 - 2022-08','1656926248','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('7','9','三鼎华乡生态园','http://webman.tinphp.com/uploads/2022/07/1656920688IMtIS.jpg','','西彭','','25元/斤（目前是早夏黑和紫蜜）','生态采摘','0','2','1','18725676661','西彭镇真武宫村三鼎华乡','2022-07 - 2022-08','1656920705','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('8','2','卓润生态园','http://webman.tinphp.com/uploads/2022/07/16569269985FuP7.jpeg','','西彭','','','','0','1','1','13368053399','西彭镇真武宫村卓润生态园','2022-07 - 2022-12','1656927001','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('9','9','蜜莉葡萄园','http://webman.tinphp.com/uploads/2022/07/1656926522OZK3k.jpg','','','','','','0','2','2','13594655321','重庆市璧山区城丁线葡萄园站','2022-07 - 2022-08','1656926524','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('10','9','玉华葡萄园','http://webman.tinphp.com/uploads/2022/07/16569267736cOXX.jpg','','璧山','','','','0','2','1','15223366708','重庆市璧山区大鹏石门村','2022-07 - 2022-08','1656926860','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('13','4','涪陵·武陵山草场','http://webman.tinphp.com/uploads/2022/07/1657525771mEpa7.jpeg','','玫瑰湾露营地','','主城—沪渝高速—长涪高速—涪陵—武陵山','露营','0','2','2','','重庆市涪陵区武陵山国家森林公园','2022-05 - 2022-11','1657874374','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('14','4','綦江·花坝草场','http://webman.tinphp.com/uploads/2022/07/1657526037I1wS5.jpeg','','','','主城—渝黔高速—安稳下道—石壕花坝','露营|赏花','0','2','2','','重庆市綦江石壕镇万隆村','2022-04 - 2022-10','1657527747','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('15','4','石柱·千野草场','http://webman.tinphp.com/uploads/2022/07/16575267899dBcP.jpeg','','','','主城—沪渝南线高速—大歇出口—S105—黄水千野草场','露营','0','2','2','','重庆市石柱县鱼池镇','2022-03 - 2022-11','1657527737','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('16','4','城口·黄安坝草场','http://webman.tinphp.com/uploads/2022/07/1657527244WRuhl.jpg','','','','主城—包茂高速—石塘下道—城万快速通道—城口—黄安坝','露营','0','2','2','','重庆市城口县东南部','','1657527786','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('17','4','云阳·岐山草场','http://webman.tinphp.com/uploads/2022/07/1657527325w60Gh.jpeg','','','','重庆—涪陵—利川—云阳龙缸·歧山草场','露营','0','2','2','','重庆云阳县清水乡龙缸风景区内','','1657527799','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('18','4','万盛·南天门','http://webman.tinphp.com/uploads/2022/07/1657527419kFDHS.jpeg','','','','主城—内环快速—兰海高速—綦万高速—南桐北路—X867—南天门','露营','0','2','2','','万盛石林镇茶园村南峰山','','1657527823','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('19','4','綦江·古剑山','http://webman.tinphp.com/uploads/2022/07/16575275274Cwzh.jpeg','','','','主城—渝黔高速—安稳下道—古剑山','帐篷|木屋|露营','0','2','2','','重庆綦江区古南街道清水村','','1657527834','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('20','4','武隆·寺院坪','http://webman.tinphp.com/uploads/2022/07/1657527581DbCY4.jpeg','','','','主城—包茂高速—南川—水江—白马—和顺镇','风车|露营','0','2','2','','重庆市武隆县兴顺乡弹子山顶','','1657527852','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('21','10','关坝镇凉风李子山','http://webman.tinphp.com/uploads/2022/07/1657591723evxZj.png','','万盛','','玫瑰香李，10元/斤','采摘|李子','0','2','1','15002322933','万盛关坝镇凉风李子山','2022-06 - 2022-07','1657591856','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('22','10','茅莱仙李','http://webman.tinphp.com/uploads/2022/07/1657592002fgJ6t.png','','璧山','','可在网上团购，2大1小入园畅吃+带走1斤9.9元','采摘|李子','0','2','2','17830218950','璧山区大兴镇大鹏场茅莱仙李','2022-06 - 2022-07','1657592064','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('23','17','茶山梨园风景区','http://webman.tinphp.com/uploads/2022/07/1657592293D41g2.png','','天坪山','','','采摘|梨子','0','2','2','','巴南区二圣镇天坪山','2022-07 - 2022-08','1657674693','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('26','14','白云蓝莓园采摘','http://webman.tinphp.com/uploads/2022/06/1656486815ZCPVG.jpeg','','木耳','','白云蓝莓园采摘，25元/斤。建议自驾前往。','蓝莓采摘','0','1','2','15502345888','重庆市渝北区木耳镇白云山社','2022-06 - 2022-08','1656920592','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('27','12','垫江西瓜种植园','http://webman.tinphp.com/uploads/2022/06/1656571862HIE3R.jpeg','','垫江','','垫江600亩西瓜成熟等你来摘','西瓜','0','1','2','','重庆市垫江县新民镇明月村西瓜种植园','2022-06 - 2022-09','1656920518','1');
INSERT INTO `t_article` (`id`,`nav_id`,`title`,`thumb`,`video`,`keywords`,`description`,`subject`,`label`,`view`,`ishot`,`introduce`,`tel`,`address`,`pick_time`,`addtime`,`status`) VALUES ('28','10','垫江万亩青脆李','http://webman.tinphp.com/uploads/2022/07/1657592707vhT5Q.png','','','','','','0','1','2','','垫江县澄溪镇十字村','2022-07 - 2022-08','1657593041','1');

-- ----------------------------
-- Table structure for t_article_content
-- ----------------------------
DROP TABLE IF EXISTS `t_article_content`;
CREATE TABLE `t_article_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '文章id',
  `content` text COMMENT '内容',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articleid` (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='文章-内容';
-- ----------------------------
-- Records of t_article_content
-- ----------------------------
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('1','1','<p>渝北·石船镇重桥村葡萄基地</p><p>又到了葡萄成熟季节，一串串晶莹剔透的葡萄挂满架，颗颗饱满，汁甜爽口。渝北区石船镇重桥村葡萄基地，邀市民去采摘葡萄，体验田园乐趣。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/16569172493l3Ge.jpg"><br></p><p>重桥村地处明月山脚下的小丘陵地带，光照充足，非常适合葡萄生长，重桥葡萄按照绿色食品标准生产，农家肥培育，均采用精细化的人工蔬果，将每串葡萄的果粒控制在100颗左右，重量在500—750g以内，采用自然晒熟等措施，从而保证重桥葡萄的品质，已荣获绿色食品称号。</p><p>具体地址：重庆市渝北区石船镇重桥村葡萄基地。</p><p>采摘价格：巨峰葡萄为每斤10元，玫瑰香葡萄为每斤15元。</p><p>咨询电话：13983834853</p><p>采摘期：预计7月初开始采摘，建议大家出行前电话 咨询</p><p>交通路线</p><p>1.重庆绕城高速—龙兴收费站下道(向右)—两江大道—石船。</p><p>2.渝邻高速—草坪收费站下道(草统路)—统景(向右)—石船。</p><p>3.机场高速—双凤桥—319国道—石船。4.内环高速——一横线——盛唐大道——石船。</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('2','27','<p>又到了西瓜成熟的季节，在重庆市垫江县新民镇的西瓜种植园里，不少游客前来采摘西瓜，在这里享受到甜蜜和清凉。</p><p><img src="http://webman.tinphp.com/uploads/2022/06/1656572244rpzDY.jpeg"><br></p><p>在垫江县新民镇明月村的西瓜地里，一排排整齐的瓜藤顺势生长，一个个瓜体匀称、表皮碧绿光滑、圆润饱满的西瓜挂在瓜藤上，长势良好，十分诱人，前来采摘的游客络绎不绝。</p><p><img src="http://webman.tinphp.com/uploads/2022/06/16565722616hZbT.jpeg"><br></p><p>市民罗浩辉说：“这是他第一次到西瓜地摘西瓜，感觉很不错，想挑选成熟的瓜，就要拍打它，如果声音比较清脆，瓜就是熟了。”瞧瞧大伙儿摘起西瓜来这高兴劲儿，甚至有朋友刚摘下来，就迫不及待地吃了起来。</p><p><img src="http://webman.tinphp.com/uploads/2022/06/1656572269yqzx7.jpeg"><br></p><p>据了解，明月村西瓜种植面积有600多亩，有玉林、美都、拿比特三个品种，目前正大量上市。这里的西瓜批发价每斤1.8元，零售价每斤2.5元左右。垫江县新民镇西瓜种植大户黄林国说：“第一期西瓜产量有2000斤左右，第二、第三、第四期的瓜在陆续上市，可以采摘到国庆节左右。”</p><p><img src="http://webman.tinphp.com/uploads/2022/06/1656572277Hja33.jpeg"><br></p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('3','26','<p><img src="http://webman.tinphp.com/uploads/2022/07/1656920578ZMHce.jpg"></p><p>重庆市渝北区木耳镇白云山社<br></p><p>白云蓝莓园采摘，25元/斤。建议自驾前往。<br></p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('4','6','<p><img src="http://webman.tinphp.com/uploads/2022/07/1656920443Vk4f5.jpg"><br></p><p><span style="font-weight: bold;">向葡萄庄园</span>这里有两百多亩地，共有不同二十多个不同的品，比如有吃葡萄不吐葡萄皮的夏黑，有形状像手指，具有葡萄中牛奶味、冰糖味、蜂蜜味的金手指，还有樱桃葡萄、茉莉香、玫瑰香等等。</p><p>乘轨道5号线到达跳蹬站，2B口出，至轨道跳蹬站公交站换乘毛线沟-西彭专线在宝华站下车，步行1.5公里到达<br></p><p>来了西彭镇，除了能够体验采摘的乐趣，还能够带回最新鲜最原生态的农产品。</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('5','7','<p><img src="http://webman.tinphp.com/uploads/2022/07/1656920698AL90q.jpg"><br></p><p>以果园种植为特色，生态种植于大棚内，天然无公害，游客可用半天到一天的时间在果园里游玩，采摘蓝莓，捕捉泥鳅，赏荷花。</p><p>品种：葡萄、李子等</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('6','8','<p>卓润生态园</p><p>占地两百余亩，以种植园区、生态池塘、荷塘观赏为特色，游客可前往园内采摘果蔬、休闲垂钓、农业观光。土鸡汤、生态鲫鱼……特色农家菜也能在这里吃到。</p><p>品种：葡萄、李子、桃子等</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('7','9','<p></p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('8','10','<p>在这50亩的大棚里</p><p>满是成熟的葡萄</p><p>现在成熟的是</p><p>夏黑和黑珍珠两个品种</p><p>下个月玫瑰香也将成熟</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('9','13','<p>武陵山不仅地势十分广阔，风景也特别漂亮。蓝天白云下，广阔的草场，奔驰的骏马，虚无缥缈的长雾云海，油彩画般的杉林晚霞，置身其中，令人心旷神怡。徜徉其中，绿草如茵，转身环顾，郁郁葱葱，各具特色的帐篷星罗密布，场面蔚为壮观。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657874350nIPg2.jpg"><br></p><p><br></p><p>地址：重庆市涪陵区武陵山国家森林公园</p><p>自驾：主城—沪渝高速—长涪高速—涪陵—武陵山</p><p><br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/16578743672As7P.png"><br></p><p><br></p><p><span style="font-weight: bold;">温馨提示</span>：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('10','14','<p>暮春夏初，花坝草场鲜花盛开，老远你就听得见牛羊、鸟儿的鸣响和叮咚的水声。这里有广袤的草场、蔽日的森林、翠绿的竹海，一切都如同人间仙境。重庆地区首家帐篷酒店便诞生在这里，而西南地区首个国家标准示范自驾车露营地也在这里出炉。</p><p>地址：重庆市綦江石壕镇万隆村</p><p>自驾：主城—渝黔高速—安稳下道—石壕花坝</p><p><br></p><p><span style="font-weight: bold;">温馨提示</span>：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('11','15','<p>千野草场位于重庆市石柱县西北部鱼池镇，景区面积6600公顷，海拔1000-1600米。千野草场景区集山、林、草、石、畜于一体，是自然观光、休闲度假的理想胜地。约上小伙伴，在这里搭个帐篷露个营，想想都是不错的体验呢。</p><p>地址：重庆市石柱县鱼池镇</p><p>自驾：主城—沪渝南线高速—大歇出口—S105—黄水千野草场</p><p><br></p><p><span style="font-weight: bold;">温馨提示</span>：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('12','16','<p><span style="font-weight: bold;">大家印象</span>：&nbsp;</p><p>黄安坝草场距重庆市城口县城70公里，是国家南方草山草坡示范开发区，总面积达30．6万亩。这宽阔的草原上，山峦起伏像座座蒙古包，白云悠悠，牛羊点点，仿佛置身于内蒙古大草原上，草场四周林海苍茫，群峰竟秀。草场远远望去，在烟云环绕的大巴山群山之颠，片片绿茵连绵起伏，仿佛一块巨大的“天上牧场”，飘飘渺渺，若隐若现。非常适合在夏秋之季，带上一家人嬉戏游玩。</p><p>城口黄安坝镶嵌于蓝天与高山之间，宽阔的草原上，山峦起伏像座座蒙古包，白云悠悠，牛羊点点，仿佛置身于内蒙古大草原上。黄安坝草场分布于大巴山主峰上，其万顷高山草场、百里原始森林非常壮观，大巴山露营基地也是露营爱好者不错的选择。</p><p><span style="font-weight: bold;">最佳季节</span>： 北亚热带山地气候；夏秋季节前往最佳<br></p><p>地址：重庆市城口县东南部</p><p>自驾：主城—包茂高速—石塘下道—城万快速通道—城口—黄安坝</p><p><br></p><p><span style="font-weight: bold;">温馨提示</span>：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('13','17','<p>歧山草场属山岳型自然风景区，为重庆市市级森林公园，岐山草场离龙缸核心景区相隔8公里左右，车程只需要10分钟。面积约12.5平方公里，海拔高度在1000米到1650米之间，景区内至今还保持着成片的国有原始森林，夏季温度只有25℃，每立方负氧离子达到2万多个。夏天放眼望去，翠峦如波、云雾如海、绿野无际、羊群如云，优越的地理位置及丰厚的自然资源无疑使得岐山草场成为人们度假、休闲、观光的好去处。</p><p>地址：重庆云阳县清水乡龙缸风景区内</p><p>自驾：重庆—涪陵—利川—云阳龙缸·歧山草场</p><p><br></p><p><span style="font-weight: bold;">温馨提示</span>：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('14','18','<p>南天门风景区位于万盛石林镇茶园村南峰山，属大娄山北部余脉，平均海拔1500多米，因地处渝黔交界，峰峦立南而得名。南天门有山有水有趣味，还有云海雾纱和清凉。夕阳西下，落日的余晖斜射在成排的风车上，风车随风轻快飞扬，让人心旷神怡。</p><p>地址：万盛石林镇茶园村南峰山</p><p>自驾：主城—内环快速—兰海高速—綦万高速—南桐北路—X867—南天门</p><p><br></p><p>温馨提示：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('15','19','<p>在綦江古剑山内，有一个面向游客开放的帐篷露营基地，风光怡人、满目苍翠、鸟语花香。里面配备了完善的营地露营设施，还有独具特色的木屋帐篷区。</p><p>地址：重庆綦江区古南街道清水村</p><p>自驾：主城—渝黔高速—安稳下道—古剑山</p><p><br></p><p>温馨提示：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('16','20','<p>不用远去风车王国荷兰，在寺院坪就能体会到浪漫的异国风情。借着蓝天白云和巨型风车的搭配，分分钟都是大片的感觉。</p><p>地址：重庆市武隆县兴顺乡弹子山顶</p><p>自驾：主城—包茂高速—南川—水江—白马—和顺镇</p><p><br></p><p>温馨提示：</p><p>露营避暑是件好事情，带齐露营装备也是必须的哟！帐篷、背包、睡袋、营灯、手电筒等，最好都不要缺，另外：</p><p>①天黑前两个小时最好选定营地，一旦天色较晚，要尽快整理一片空地；</p><p>②灯、手机放在身边随手可取的位置；</p><p>③如果遇上泥石流，切忌沿沟向上或向下跑，应向两侧山坡跑，快速离开河道、溪谷，不要在土质松软的斜坡处停留。</p><p>④露营为户外旅游活动，在畅想清凉的同时，游友们更要注意安全！</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('17','21','<p>凉风李子园从2013年开始发展李子种植栽种玫瑰香李、凉风脆李、布郎李、红美人4个品种，还有冰糖麒麟瓜和最新引进种植的凉风油桃。目前，李子园面积已发展至200余亩，年产量达4万余斤。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657591756JGbr7.jpg"><br></p><p>玫瑰香李，果实特大，果皮中厚，着色全面鲜红，果肉橙黄色，细嫩，清脆爽口，汁液丰富，风味酸甜适中，有香味，品质上等。</p><p>凉风脆李，果形端庄、质地脆嫩、汁多味香，果肉浅黄色、肉质致密，纤维少，汁多味香，脆嫩，酸甜适口。</p><p>布朗李，饱满圆润，玲珑剔透形态美艳，口味甘甜成熟时果皮黑色果肉暗红色，果质优良。</p><p>红美人，果皮紫红色，果肉黄色或偶带片状红色，果粉厚，灰白色，肉质脆，味甜。</p><p>目前，凉风李子园玫瑰香李已成熟，接下来凉风脆李、布郎李、红美人三个品种将陆续成熟，还有凉风油桃、冰糖麒麟瓜也将在六月迎来采摘最佳时间。</p><p>7月2日-7月3日，李子采摘节将在关坝镇凉风村拉开帷幕，4万斤新鲜李子硕果挂枝，邀请游客前来观赏拍照，体验采摘乐趣，零距离品尝凉风李子的独特味道。</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('18','22','<p>茅莱仙李果园位于璧山大兴镇大鹏场，整个采摘园占地100多亩，有几千棵李子果树，现如今，已经挂满了紫色饱满的凤凰李。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592010WLqFJ.jpg"><br></p><p>说起李子的品种，大多数人熟悉的还是脆红李、江安李…… 如果你是第一次听说凤凰李，建议你一定要找机会试试，没准会开启你对李子认知的新大门。</p><p>地址：璧山区大兴镇大鹏场（茅莱仙李）</p><p>联系电话：17830218950/13883048402</p><p>价格：可在网上团购，2大1小入园畅吃+带走1斤9.9元</p><p>使用时间：即日起-7月10日</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('19','23','<p>梨香迎面来，万树梨果枝头挂。重庆市巴南区二圣镇天坪山上6000余亩二圣梨已经成熟，迎来至八月中旬的采摘期。一颗颗圆润、硕大的果实沉甸甸地挂在枝头，摘下一个放在手里分量感十足，一口咬下去，甜甜的果汁瞬间流入口中。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/165759248648Kbl.jpg"><br></p><p>二圣镇天坪山二圣梨种植地。<br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592508sXAD0.png"><br></p><p>已成熟的二圣梨。<br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592520wiOTk.png"><br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592552bM2rs.png"><br></p><p>正在采摘<br></p><p>二圣梨”是以翠冠梨为主要品种的早熟梨，其果肉白色、肉质细嫩、入口化渣、汁多味甜，先后被国家知识产权局和农业农村部评为“双地标”产品。今年预计产量为750万斤。<br></p><p><br></p><p>转载：https://view.inews.qq.com/a/20220708A05HJ000?refer=wx_hot</p>');
INSERT INTO `t_article_content` (`id`,`article_id`,`content`) VALUES ('20','28','<p>眼下正是李子成熟时节，垫江县1.2万亩青脆李喜获丰收，累累果实挂满枝头。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592887H00Hh.jpeg"><br></p><p>垫江县澄溪镇十字村的易军便带着村民到自家的果园采摘李子。易军告诉记者，他家栽种了100多亩李子树，今年全部挂果。易军说，今年天气好，李子产量比较高，每亩能产400多斤，个头大的李子能卖到25元一斤，每亩能收入6000元左右。<br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592908VwuuI.jpeg"><br></p><p>澄溪镇十字村青脆李种植已有十余年，得天独厚的地理优势和气候条件为青脆李提供了极佳的生长空间，这里的青脆李不仅产量高，品相好，而且挂果时间长。每天前来采摘的商贩和市民络绎不绝。大家品尝香甜脆李，体验采摘乐趣，尽情享受着丰收的喜悦。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/16575929261QdQZ.jpeg"><br></p><p>游客刘群说，这次摘了二十多斤李子，这里的李子又脆又甜，她每年都要来采摘。</p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592937T5u5H.jpeg"><br></p><p>易军介绍，现在主要是网上销售、现场采摘、市民认养果树，除了青脆李，他们还建立了绿色食品体验馆，种植当季蔬菜，养殖了鸡鸭鱼羊等土货，游客来到这里既能采摘到新鲜的李子，又能品尝到原汁原味的农家风味。<br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592947QeAex.jpeg"><br></p><p><img src="http://webman.tinphp.com/uploads/2022/07/1657592958FEZk4.jpeg"><br></p><p>垫江县把青脆李作为特色农业品牌发展，引导种植户成立专业合作社，并加强技术培训，提高青脆李种植的科技含量，提升产量和质量。今年全县青脆李挂果面积达到1.2万亩，产量达1.44万吨，预计实现产值达5700多万元。<br></p><p><br></p><p>转载：https://www.cbg.cn/show/4933-2154587.html</p>');

-- ----------------------------
-- Table structure for t_banner
-- ----------------------------
DROP TABLE IF EXISTS `t_banner`;
CREATE TABLE `t_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) DEFAULT '',
  `thumb` char(200) DEFAULT '',
  `url` char(100) DEFAULT '',
  `sort` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1正常，2禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='广告';
-- ----------------------------
-- Records of t_banner
-- ----------------------------
INSERT INTO `t_banner` (`id`,`name`,`thumb`,`url`,`sort`,`status`) VALUES ('2','测试1','http://webman.tinphp.com/uploads/2022/06/1656470175PjJSD.jpg','','0','1');
INSERT INTO `t_banner` (`id`,`name`,`thumb`,`url`,`sort`,`status`) VALUES ('3','测试2','http://webman.tinphp.com/uploads/2022/07/1657593947t0BGz.jpeg','','0','1');
INSERT INTO `t_banner` (`id`,`name`,`thumb`,`url`,`sort`,`status`) VALUES ('4','测试3','http://webman.tinphp.com/uploads/2022/07/1657594121GsF0L.jpeg','','0','1');
INSERT INTO `t_banner` (`id`,`name`,`thumb`,`url`,`sort`,`status`) VALUES ('5','测试4','http://webman.tinphp.com/uploads/2022/07/1657594329MEgb0.png','','0','1');
INSERT INTO `t_banner` (`id`,`name`,`thumb`,`url`,`sort`,`status`) VALUES ('6','测试5','http://webman.tinphp.com/uploads/2022/07/165759327466Ncy.png','','0','1');

-- ----------------------------
-- Table structure for t_config
-- ----------------------------
DROP TABLE IF EXISTS `t_config`;
CREATE TABLE `t_config` (
  `name` varchar(250) NOT NULL,
  `data` varchar(500) NOT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统设置';
-- ----------------------------
-- Records of t_config
-- ----------------------------
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_title','GoPick','站点标题');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_subtitle','webman','站点副标题');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_url','http://webman.tinphp.com/','站点网址');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_keywords','采摘网','站点关键词');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_description','webman+layui后台系统','站点描述');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_email','重庆市渝北区冉家坝','站长邮箱');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_copyright','采摘网','站点版权');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('site_statistics','','站点统计');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('uploads_status','3','上传设置,1本地,2七牛');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('tpl_name','default','站点模板');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('tpl_index','index','首页模板');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('tpl_search','search','搜索模板');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('mobile_status','2','开启手机版,1开启,2关闭');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('mobile_tpl','','站点模板');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('mobile_domain','','绑定域名');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('al_sms_akid','LTAI5t6xT1k73ytdsKUu7Pgi','AccessKeyId');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('al_sms_akst','vCzU6bmeUfKYVKROIdbxEqMQWrchGY','AccessKeySecret');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('al_sms_sname','','签名名称');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('al_sms_temp1','SMS_215375253','注册');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('al_sms_temp2','SMS_215375255','登录');
INSERT INTO `t_config` (`name`,`data`,`note`) VALUES ('al_sms_temp3','SMS_215375256','通用');

-- ----------------------------
-- Table structure for t_config_upload
-- ----------------------------
DROP TABLE IF EXISTS `t_config_upload`;
CREATE TABLE `t_config_upload` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `upload_size` int(10) NOT NULL COMMENT '文件大小',
  `upload_exts` varchar(250) DEFAULT NULL COMMENT '文件类型',
  `thumb_status` tinyint(1) DEFAULT '1' COMMENT '是否重新绘制上传生产的图片,1开启,2关闭',
  `thumb_width` int(10) DEFAULT NULL COMMENT '生产图片尺寸,宽',
  `thumb_height` int(10) DEFAULT NULL COMMENT '生产图片尺寸,高',
  `water_status` tinyint(1) DEFAULT '1' COMMENT '水印类型,1文字,2图片',
  `water_image` varchar(250) DEFAULT NULL COMMENT '水印',
  `water_position` int(10) DEFAULT '1' COMMENT '水印位置,1左上,2左下,3右上,4右下,5中间',
  `water_prefix` char(200) DEFAULT NULL COMMENT '图片前缀',
  `qn_ak` char(100) DEFAULT '' COMMENT '七牛accessKey',
  `qn_sk` char(100) DEFAULT '' COMMENT '七牛secretKey',
  `qn_bucket` char(100) DEFAULT '' COMMENT '上传空间',
  `qn_domain` char(100) DEFAULT '' COMMENT '绑定域名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='网站配置-上传';
-- ----------------------------
-- Records of t_config_upload
-- ----------------------------
INSERT INTO `t_config_upload` (`id`,`upload_size`,`upload_exts`,`thumb_status`,`thumb_width`,`thumb_height`,`water_status`,`water_image`,`water_position`,`water_prefix`,`qn_ak`,`qn_sk`,`qn_bucket`,`qn_domain`) VALUES ('1','10','jpg,gif,bmp,png','2','800','800','2','tin','4','','bpM4mYnptD8-CJU4pinhcalz3UTlduMdD55PiN8i','IZ9YSqdtlfxC3UuPi9kXSQXu9URpehSrFBBqDtZG','','http://file.dianyinhuoban.vip');

-- ----------------------------
-- Table structure for t_guide
-- ----------------------------
DROP TABLE IF EXISTS `t_guide`;
CREATE TABLE `t_guide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) DEFAULT '' COMMENT '名称',
  `url` char(100) DEFAULT '' COMMENT '链接',
  `sort` int(11) DEFAULT '50' COMMENT '排序',
  `addtime` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，2锁定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='引导';
-- ----------------------------
-- Records of t_guide
-- ----------------------------
INSERT INTO `t_guide` (`id`,`name`,`url`,`sort`,`addtime`,`status`) VALUES ('1','tinphp','http://www.tinphp.com/','1','1653546042','1');
INSERT INTO `t_guide` (`id`,`name`,`url`,`sort`,`addtime`,`status`) VALUES ('2','198book','http://www.198book.com/','2','1653546076','1');
INSERT INTO `t_guide` (`id`,`name`,`url`,`sort`,`addtime`,`status`) VALUES ('4','接口管理系统','http://rap.tinphp.com/','4','1653551707','1');
INSERT INTO `t_guide` (`id`,`name`,`url`,`sort`,`addtime`,`status`) VALUES ('5','文件管理系统','http://file.tinphp.com/','5','1653551660','1');
INSERT INTO `t_guide` (`id`,`name`,`url`,`sort`,`addtime`,`status`) VALUES ('6','宝塔','http://123.58.217.105:1988/06c24f59','3','1654074942','1');
INSERT INTO `t_guide` (`id`,`name`,`url`,`sort`,`addtime`,`status`) VALUES ('7','首页','http://www.gopick.cn/','50','1656321260','1');

-- ----------------------------
-- Table structure for t_link
-- ----------------------------
DROP TABLE IF EXISTS `t_link`;
CREATE TABLE `t_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(100) DEFAULT '' COMMENT '名称',
  `url` char(100) DEFAULT '' COMMENT '链接',
  `sort` int(11) DEFAULT '50' COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1正常，2禁用',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='友情链接';
-- ----------------------------
-- Records of t_link
-- ----------------------------
INSERT INTO `t_link` (`id`,`title`,`url`,`sort`,`status`,`addtime`) VALUES ('1','百度','https://www.baidu.com/','1','1','1653364719');

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT '0',
  `type` tinyint(1) DEFAULT '1' COMMENT '分类，1登录，2添加，3编辑，4删除',
  `note` char(255) DEFAULT '' COMMENT '内容',
  `ip` char(50) DEFAULT '',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户日志';
-- ----------------------------
-- Records of t_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_nav
-- ----------------------------
DROP TABLE IF EXISTS `t_nav`;
CREATE TABLE `t_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) DEFAULT '0' COMMENT '栏目级别,0为顶级栏目,其它就为子栏目',
  `name` char(50) DEFAULT '' COMMENT '栏目名称',
  `sort` int(11) DEFAULT '50' COMMENT '排序',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型：1列表，2单页，外链',
  `action` char(200) DEFAULT '' COMMENT '单页id/外链链接',
  `tpl_list` char(100) DEFAULT '' COMMENT '模板-列表',
  `tpl_page` char(100) DEFAULT '' COMMENT '模板-详情',
  `ishow` tinyint(1) DEFAULT '1' COMMENT '显示导航，1显示，2隐藏',
  `islow` tinyint(1) DEFAULT '2' COMMENT '显示导航，1显示，2隐藏',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1正常，2禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='栏目菜单';
-- ----------------------------
-- Records of t_nav
-- ----------------------------
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('1','0','水果采摘','1','1','home/news/index','article/pick','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('2','0','蔬菜采摘','2','1','home/news/index','article/pick','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('3','0','采摘点评','3','1','home/news/index','article/comment','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('4','0','小众野营','4','1','','article/camp','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('5','1','橘子','1','1','home/news/index','article/pick','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('6','1','樱桃','2','1','home/news/index','article/pick','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('8','0','关于我们','5','2','1','','page/info','2','1','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('9','1','葡萄','3','1','home/news/index','article/pick','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('10','1','李子','4','1','home/news/index','','','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('11','1','桃子','5','1','home/news/index','','','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('12','1','西瓜','6','1','home/news/index','','','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('13','1','草莓','7','1','','','','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('14','1','蓝莓','8','1','','','article/info','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('15','1','杨梅','9','1','','','','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('16','1','枇杷','10','1','','','','1','2','1');
INSERT INTO `t_nav` (`id`,`fid`,`name`,`sort`,`type`,`action`,`tpl_list`,`tpl_page`,`ishow`,`islow`,`status`) VALUES ('17','1','梨子','11','1','','article/pick','article/info','1','2','1');

-- ----------------------------
-- Table structure for t_page
-- ----------------------------
DROP TABLE IF EXISTS `t_page`;
CREATE TABLE `t_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(100) DEFAULT '' COMMENT '标题名称',
  `keywords` char(100) DEFAULT '' COMMENT '关键字',
  `description` char(200) DEFAULT '' COMMENT '描述',
  `content` longtext COMMENT '内容',
  `view` int(11) DEFAULT '0' COMMENT '点击量浏览量阅读量',
  `addtime` int(11) DEFAULT '0' COMMENT '发布时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，2禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='单页';
-- ----------------------------
-- Records of t_page
-- ----------------------------
INSERT INTO `t_page` (`id`,`title`,`keywords`,`description`,`content`,`view`,`addtime`,`status`) VALUES ('1','关于我们','关于我们','关于我们','<p>发动反攻和法国</p>','48','1656123981','1');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(100) DEFAULT '' COMMENT '账号',
  `password` char(100) DEFAULT '' COMMENT '密码',
  `salt` char(100) DEFAULT '' COMMENT '密码盐',
  `token` char(100) DEFAULT '',
  `nickname` char(50) DEFAULT '' COMMENT '昵称',
  `thumb` char(100) DEFAULT '' COMMENT '缩略图',
  `name` char(20) DEFAULT '' COMMENT '姓名',
  `phone` char(20) DEFAULT '' COMMENT '手机号',
  `ip` char(50) DEFAULT '' COMMENT '注册ip',
  `addtime` int(11) DEFAULT '0' COMMENT '注册时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，2冻结',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会员';
-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` (`id`,`username`,`password`,`salt`,`token`,`nickname`,`thumb`,`name`,`phone`,`ip`,`addtime`,`status`) VALUES ('1','tianji','579c87d624f577e5ee39d3d71a99757b','v385xhnj4cp2b0yir9afmw','','一杯浊酒','http://webman.tinphp.com/uploads/2022/05/1653449139gT5ob.jpeg','蜗牛','','','1653449189','1');

-- ----------------------------
-- Table structure for t_user_log
-- ----------------------------
DROP TABLE IF EXISTS `t_user_log`;
CREATE TABLE `t_user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `type` tinyint(1) DEFAULT '1' COMMENT '分类，1注册，2登录，3编辑信息',
  `note` char(255) DEFAULT '' COMMENT '内容',
  `ip` char(50) DEFAULT '',
  `addtime` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员日志';
-- ----------------------------
-- Records of t_user_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_visit
-- ----------------------------
DROP TABLE IF EXISTS `t_visit`;
CREATE TABLE `t_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` char(50) DEFAULT '',
  `addtime` int(11) DEFAULT '0',
  `url` char(200) DEFAULT '' COMMENT '请求url',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='来访记录';
-- ----------------------------
-- Records of t_visit
-- ----------------------------
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('1','14.108.214.60','1657676004','//www.gopick.cn/article/info?id=23');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('2','222.177.14.235','1657676040','//www.gopick.cn/article/info?id=23');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('3','14.108.214.60','1657676351','//www.gopick.cn/article/info?id=23');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('4','14.108.214.60','1657676431','//www.gopick.cn/article/info?id=23');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('5','14.108.214.60','1657676848','//www.gopick.cn/article/info?id=23');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('6','121.4.79.217','1657734868','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('7','111.13.63.178','1657882977','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('8','111.13.63.100','1657883063','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('9','47.102.43.165','1657905026','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('10','47.102.149.15','1657907951','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('11','111.13.63.68','1657962554','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('12','106.75.176.113','1657962571','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('13','111.7.100.22','1657976492','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('14','111.7.100.21','1657976514','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('15','111.7.100.20','1657976517','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('16','36.99.136.132','1657976535','//www.gopick.cn/article/info?id=10');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('17','111.7.100.24','1657976535','//www.gopick.cn/page/index?id=8');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('18','111.7.100.23','1657976535','//www.gopick.cn/article/info?id=27');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('19','111.7.100.25','1657976535','//www.gopick.cn/article/list?id=3');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('20','111.7.100.22','1657976535','//www.gopick.cn/article/info?id=28');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('21','111.7.100.26','1657976535','//www.gopick.cn/article/info?id=8');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('22','111.7.100.27','1657976536','//www.gopick.cn/article/list?id=4');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('23','111.7.100.20','1657976536','//www.gopick.cn/article/list?id=2');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('24','36.99.136.140','1657976536','//www.gopick.cn/article/info?id=21');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('25','111.7.100.27','1657976536','//www.gopick.cn/article/info?id=26');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('26','36.99.136.133','1657976536','//www.gopick.cn/article/list?id=1');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('27','111.7.100.22','1657976537','//www.gopick.cn/article/info?id=7');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('28','36.99.136.134','1657976543','//www.gopick.cn/article/info?id=10');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('29','36.99.136.130','1657976548','//www.gopick.cn/article/info?id=10');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('30','36.99.136.141','1657976550','//www.gopick.cn/article/info?id=21');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('31','36.99.136.139','1657976556','//www.gopick.cn/article/info?id=21');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('32','111.7.100.26','1657976567','//www.gopick.cn/article/info?id=26');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('33','111.7.100.26','1657976568','//www.gopick.cn/article/list?id=3');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('34','111.7.100.26','1657976573','//www.gopick.cn/article/info?id=26');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('35','111.7.100.25','1657976574','//www.gopick.cn/article/list?id=4');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('36','111.7.100.21','1657976576','//www.gopick.cn/article/info?id=7');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('37','111.7.100.24','1657976576','//www.gopick.cn/article/info?id=8');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('38','111.7.100.27','1657976576','//www.gopick.cn/page/index?id=8');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('39','111.7.100.23','1657976578','//www.gopick.cn/article/list?id=2');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('40','111.7.100.26','1657976578','//www.gopick.cn/article/list?id=3');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('41','111.7.100.23','1657976579','//www.gopick.cn/article/info?id=7');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('42','111.7.100.25','1657976581','//www.gopick.cn/page/index?id=8');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('43','111.7.100.24','1657976584','//www.gopick.cn/article/info?id=8');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('44','111.7.100.23','1657976586','//www.gopick.cn/article/info?id=27');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('45','111.7.100.23','1657976590','//www.gopick.cn/article/info?id=28');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('46','111.7.100.22','1657976592','//www.gopick.cn/article/info?id=27');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('47','111.7.100.23','1657976592','//www.gopick.cn/article/list?id=2');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('48','111.7.100.23','1657976597','//www.gopick.cn/article/info?id=28');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('49','119.188.3.101','1657993131','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('50','47.102.124.27','1658164544','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('51','42.83.147.34','1658166902','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('52','61.135.164.102','1658418904','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('53','47.103.140.59','1658423378','//www.gopick.cn/');
INSERT INTO `t_visit` (`id`,`ip`,`addtime`,`url`) VALUES ('54','42.83.147.41','1658430672','//www.gopick.cn/');

