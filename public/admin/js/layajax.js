layui.define(['jquery','element','layer','form'], function(exports){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element
	,form = layui.form;
	
	//监听switch开关
	//<input type="checkbox" lay-skin="switch" lay-filter="switch" value="" lay-text="正常|禁用" data-default="1|2" data-url=''>
	form.on('switch(switch)', function(data) {
		var url = $(this).data('url');		//必填 data-url
		var def = $(this).data('default');	//必填 data-default	开关默认真假值 例[1|2]
		var change = data.elem.checked;		//开关是否开启，true或者false
		
		//后台我需要的是1或2，所以预先在js中处理change的值
		if(def){
			var ary = def.split("|");
			if(change==true){
				change = ary[0];
			}else{
				change = ary[1];
			}
		}else{
			if(change==true){
				change = 1;
			}else{
				change = 2;
			}
		}
		
		$.post(url, {field_value: change}, function(res) {
			console.log(res);
			if(res.code == 1) {
				layer.msg(res.msg,{icon:1,time:2000});
			} else {
				layer.msg(res.msg);
			}			
		});
	});
	
	//监听input排序 失去焦点后触发
	//<input class='ajax-sort' data-url='' data-value='' value=''>
	$('.ajax-sort').on('blur', function(data){
		var url = $(this).data('url');
		var datavalue = $(this).data('value');
		var fieldvalue = $(this).val();
		
		//比较值是否发生改变
		if(datavalue!=fieldvalue){
			$.post(url, {field_value: fieldvalue}, function(res) {
				console.log(res);
				if(res.code == 1) {
					layer.msg(res.msg,{icon:1,time:2000}, function(index){
						location.reload();
					});
				} else {
					layer.msg(res.msg);
				}			
			});
		}
	});
	
	//监听删除行为
	//<a class="ajax-del" data-url='{:url("del")}' data-field='id' data-value=''>删除</a>
	$('.ajax-del').on('click', function(){
		var url = $(this).data('url');
		var field = $(this).data('field');
		var value = $(this).data('value');
		layer.open({
			type: 1
			,title:'提示'
			,content: '<div style="padding:20px 100px;">确认 删除 操作吗</div>'
			,btn: '确定'
			,btnAlign: 'c' //按钮居中
			,shade: 0 //不显示遮罩
			,yes: function(index){
				var data = {};
				data[field] = value; 
				
				$.ajax({
					url: url,
					data: data,
					type: "post",
					dataType:'json',
					success:function(res){
						if(res.code==undefined){
							console.log('res为json字符串',res);
							res = JSON.parse(res);
						}else{
							console.log('res为json对象',res);
						}
						if(res.code == 1) {
							layer.close(index);
							layer.msg(res.msg,{icon:1,time:2000}, function(index){
								location.reload();
							});
						} else {
							layer.close(index);
							layer.msg(res.msg);
						}
					},
					error: function(res){
						console.log(res);
						layer.msg('致命错误');
					}
				});
			}
		});	
	});
	
	//复选框 全选/取消
	form.on('checkbox(idselall)', function(data){
		var name = $(this).attr('data-name');
		console.log('name',name);
		
		if(name==''||name=='undefined'||name==null||name==false){
			$('.layui-table input[name=id]:checkbox').each(function(index, item){
				item.checked = data.elem.checked;
			});
		}else{
			$('.layui-table input[name='+name+']:checkbox').each(function(index, item){
				item.checked = data.elem.checked;
			});
		}		
		
		form.render('checkbox');
	});
	
	//确认后执行操作
	//<a class="layui-btn layui-btn-xs ajax-sure" data-url="admin/finance/ajax_reject" data-field="id" data-value="1" data-edit="status|2">驳回</a>
	$('.ajax-sure').on('click',function(){
		var title = $(this).html();
		var url = $(this).data('url');
		var field = $(this).data('field');
		var value = $(this).data('value');
		var edit = $(this).data('edit');
		
		console.log(title,url,field,value);
		
		layer.open({
			type: 1
			,title:'提示'
			,content: '<div style="padding:20px 100px;">确认 '+ title +' 操作吗</div>'
			,btn: '确定'
			,btnAlign: 'c' //按钮居中
			,shade: 0 //不显示遮罩
			,yes: function(index){
				var map = {};
				map[field] = value;
				if(edit){
					var ary = edit.split("|");
					map[ary[0]] = ary[1];
				}

				$.post(url, map, function (res) {
					if(res.code==undefined){
						console.log('res为json字符串',res);
						res = JSON.parse(res);
					}else{
						console.log('res为json对象',res);
					}
					if(res.code == 1) {
						layer.close(index);
						layer.msg(res.msg,{icon:1,time:2000}, function(index){
							location.reload();
						});
					} else {
						layer.close(index);
						layer.msg(res.msg);
					}
				});
			}
		});
	});
	
	//页面级菜单先执行后弹窗提示
	$('.ajax-offset').on('click',function(){
		var url = $(this).data('url');
		var title = $(this).html();
		var field = $(this).data('field');
		var value = $(this).data('value');
		
		console.log(title,url,field,value);
		
		var map = {};
		if(field&&value){
			map[field] = value; 
		}else{
			map['id'] = 1; 
		}
		
		$.post(url, map, function (res) {
			console.log(res);
			
			layer.open({
				type: 1
				,title:title
				,content: '<div style="padding:20px 100px;">'+res+'</div>'
				,btn: '确定'
				,btnAlign: 'c' //按钮居中
				,shade: 0 //不显示遮罩
				,yes: function(index){
					layer.close(index);
				}
			});
			
		});
	});
	
	exports('layajax', {});
});