layui.define(['jquery','element','layer','form'], function(exports){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element
	,form = layui.form;
	
	//初始化右侧内容区域高度
	var divh = 0;
	divh = $(".layui-body").height();
	divh = divh - 42;
	$(".layui-body #admin-tab .layui-tab-content .layui-tab-item").css("height",divh+"px");
	//监听浏览器变化
	window.onresize = function(){
		//动态设置框架的高
		divh = $(".layui-body").height();
		divh = divh - 42;
		
		console.log(divh);
		$(".layui-body #admin-tab .layui-tab-content #right-content").css("height",divh+"px");  
	}
	/* 结束 初始化右侧内容区域高度 */

    //左侧菜单折叠
	$('.kzt').on('click', function(){
		var fold = $(this).attr('fold');
		console.log('fold=',fold);
		if(fold=='1'){
			$('#left-nav').css({"cssText":"width:50px !important;overflow:hidden"});
			//$('#left-nav span').css({"cssText":"display:none !important;"});
			
			$(this).attr('fold','2');
			$('.layui-body').css('left','60px');
		}else{
			$('#left-nav').css({"cssText":"width:200px !important;overflow:hidden"});
			//$('#left-nav span').css({"cssText":"display:inline !important;"});
			
			$(this).attr('fold','1');
			$('.layui-body').css('left','200px');
		}
		window.frames["mhsh_iframe"].location.reload(true);
	});
	
	//刷新iframe
	$('.layui-refresh').on('click', function(){
		window.frames["mhsh_iframe"].location.reload(true);
	});
	
    exports('tabmenu', function(options) {
        //var navtab = new tabMenu();
        //return navtab.set(options)
    });
	
});