<?php

namespace app\controller;

use support\Request;
use Gregwar\Captcha\CaptchaBuilder;
use app\model\AdminRole;

class Index extends Base
{
    public function index(Request $request)
    {
        return $this->fetch('index/index');
    }

    /**
     * 输出验证码图像
     */
    public function captcha(Request $request)
    {
        // 初始化验证码类
        $builder = new CaptchaBuilder;
        // 生成验证码
        $builder->build();
        // 将验证码的值存储到session中
        $request->session()->set('captcha', strtolower($builder->getPhrase()));
        // 获得验证码图片二进制数据
        $img_content = $builder->get();
        // 输出验证码二进制数据
        return response($img_content, 200, ['Content-Type' => 'image/jpeg']);
    }

    public function login(Request $request)
    {
        $username = $request->post('username');
        $password = $request->post('password');
        $captcha  = $request->post('captcha');

        if ($username == null || $password == null) {
            return return_json(0, '用户名和密码不能为空');
        }

        // 对比session中的captcha值
        if (strtolower($captcha) !== $request->session()->get('captcha')) {
            return return_json(0, '输入的验证码不正确');
        }

        try {
            $modelAdmin = model('Admin');
        } catch (\Throwable $e) {
            //return return_json(0, $e->getMessage());
            return return_json(0, '异常错误');
        }

        $where['username'] = $username;
        $where['status'] = 1;
        $infoAdmin = $modelAdmin->where($where)->find();
        if (empty($infoAdmin)) {
            return return_json(0, '用户名不存在');
        }

        $new_password = md5(md5($password) . $infoAdmin['salt'] . $infoAdmin['id']);
        if ($infoAdmin['password'] != $new_password) {
            return return_json(0, '密码错误'.$new_password);
        }

        $token = md5(rand(100000, 999999) . time());
        $temp = array(
            'id' => $infoAdmin['id'],
            'username' => $infoAdmin['username'],
            'nickname' => $infoAdmin['nickname'],
            'token' => $token,
            'roleId' => $infoAdmin['roleId'],
            'sorttime' => time()
        );
        $modelAdmin->update($temp);

        //添加session
        $session = $request->session();
        $session->put($temp);

        //缓存组权限信息
        $modelAdminRole = new AdminRole;
        $listRole = $modelAdminRole->getList();
        cache('role_list', $listRole);

        $temp['url'] = url('home/index');
        return return_json(1, '登录成功', $temp);
    }

    /**
     * 退出登录
     */
    public function logout(Request $request)
    {
        $session = $request->session();

        try {
            $modelAdmin = model('Admin');
        } catch (\Exception $e) {
            return return_json(0, '异常错误');
        }
        $modelAdmin->where('id', '=', $session->has('id'))->update(['token' => '']);

        $request->session()->flush();
        if ($session->has('id')) {
            $session->forget(['id', 'username', 'nickname', 'token', 'roleId', 'sorttime']);
        }

        return return_json(2, '退出系统成功！', url('index/index'));
    }

    /*
     * 一键清空缓存
     */
    public function delcache()
    {
        $path = '/runtime';
        delDirAndFile($path);
        return return_json(1, '缓存清除成功');
    }

}
