<?php

namespace app\controller;

use support\Request;
use app\model\Nav as navModel;

class Nav extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '导航管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('nav/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('nav/info'),
                ),
            ),
        );
    }

    public function index(Request $request)
    {
        //筛选条件
        $where = array();

        $id = $request->input('id');
        if ($id) {
            $where['id'] = $id;
        }
        if(empty($where)){
            $where['fid'] = 0;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['id'] = $id;

        //查询数据
        $navModel = new navModel;
        $list = $navModel->getListSub($where);
        $this->assign('list', $list);
        $this->assign('pageMaps', $pageMaps);

        return view('nav/index');
    }

    public function info(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $navModel = new navModel;
        if ($method == 'POST') {
            if ($id) {
                $status = $navModel->editAll($request);
            } else {
                $status = $navModel->addall($request);
            }
            if ($status !== false) {
                return return_json(1, '操作成功', url('nav/index'));
            } else {
                return return_json(0, '操作失败');
            }
        } else {
            $info = $navModel->getInfo($id);
            $this->assign('info', $info);

            //只有列表类型才能添加下级栏目
            $topdata['fid'] = 0;
            $topdata['status'] = 1;
            $topdata['type'] = 1;
            $list = $navModel->getListSub($topdata);
            $this->assign('list', $list);

            return view('nav/info');
        }
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $navModel = new navModel;
        if ($navModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
