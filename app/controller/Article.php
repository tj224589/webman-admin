<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\Article as articleModel;
use app\model\Nav;

class Article extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '文章管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('article/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('article/info'),
                ),
            ),
        );
    }

    public function index(Request $request)
    {
        //筛选条件
        $where = array();

        $id = $request->input('id');
        $nav_id = $request->input('nav_id');
        $ishot = $request->input('ishot');
        $introduce = $request->input('introduce');
        $status = $request->input('status');

        if ($id) {
            $where['id'] = $id;
        }
        if ($nav_id) {
            $where['nav_id'] = $nav_id;
        }
        if ($ishot) {
            $where['ishot'] = $ishot;
        }
        if ($introduce) {
            $where['introduce'] = $introduce;
        }
        if ($status) {
            $where['status'] = $status;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['id'] = $id;
        $pageMaps['nav_id'] = $nav_id;
        $pageMaps['ishot'] = $ishot;
        $pageMaps['introduce'] = $introduce;
        $pageMaps['status'] = $status;

        $pageAry = [
            'list_rows' => 20,
            'page' => $request->input('page', 1),
            'path' => '/article/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $articleModel = new articleModel;
        $navModel = new Nav;

        $list = $articleModel->loadList($where, $pageAry);
        if (!empty($list)) {
            foreach ($list as $key => $val) {
                $mapNav['id'] = $val['nav_id'];
                $val['nav_name'] = $navModel->getWhereValue($mapNav, 'name');
            }
        }
        View::assign('list', $list);
        View::assign('_page', $list->render());
        View::assign('pageMaps', $pageMaps);

        $whereNav['fid'] = 0;
        $whereNav['type'] = 1;
        $whereNav['status'] = 1;
        $listlm = $navModel->getListSub($whereNav);
        $this->assign('listlm', $listlm);

        return view('article/index');
    }

    public function info(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $articleModel = new articleModel;
        if ($method == 'POST') {
            if ($id) {
                $status = $articleModel->editAll($request);
            } else {
                $status = $articleModel->addall($request);
            }
            if ($status !== false) {
                return return_json(1, '操作成功', url('article/index'));
            } else {
                return return_json(0, '操作失败');
            }
        } else {
            $info = $articleModel->getInfo($id);
            View::assign('info', $info);

            $mapNav['fid'] = 0;
            $mapNav['type'] = 1;
            $mapNav['status'] = 1;
            $navModel = new Nav;
            $listlm = $navModel->getListSub($mapNav);
            View::assign('listlm', $listlm);

            return view('article/info');
        }
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $articleModel = new articleModel;
        if ($articleModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
