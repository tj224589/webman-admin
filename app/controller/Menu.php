<?php
namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\AdminMenu;

class Menu extends Base
{
	protected function _infoModule(){
        return array(
            'info' => array(
                'name' => '管理菜单',
                'description' => '站点运行信息',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('menu/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('menu/info'),
                ),
            ),
        );
    }
	
    public function index(Request $request){
		$modelAdminMenu = new AdminMenu;
        $list = $modelAdminMenu->loadList();
		View::assign('list', $list);
		
		return view('menu/index');
    }
	
    //后台菜单
    public function info(Request $request){
		$method = $request->method();
        $id = $request->input('id');
        $modelAdminMenu = new AdminMenu;
        if ($method == 'POST'){
            if ($id){
                $status = $modelAdminMenu->editAll($request);
            }else{print_r('-1-');
                $status = $modelAdminMenu->addall($request);
				print_r($status);print_r('-2-');
            }
            if($status!==false){
                return return_json(1,'操作成功',url('menu/index'));
            }else{
                return return_json(0,'操作失败');
            }
        }else{
            $info = $modelAdminMenu->getInfo($id);
            View::assign('info', $info);
			
			if(empty($info['act'])){
				$start_act = 1;
			}else{
				$start_act = count($info['act']) + 1;
			}
			View::assign('start_act',$start_act);
			
            View::assign('menuList',$modelAdminMenu->loadList());
			
            return view('menu/info');
        }
    }
	
    //菜单删除
    public function del(Request $request){		
        $id = $request->input('id');
        if (empty($id)){
            return return_json(0,'参数不能为空');
        }
		
		$modelAdminMenu = new AdminMenu;
        //判断子栏目
        if($modelAdminMenu->loadList(array(), $id)){
            return return_json(0,'请先删除子菜单！');
        }
        //删除栏目操作
        if($modelAdminMenu->del($id)){
            return return_json(1,'栏目删除成功！');
        }else{
            return return_json(0,'栏目删除失败！');
        }
    }

}
