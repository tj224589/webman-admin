<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\Banner as bannerModel;

class Banner extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '广告管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('banner/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('banner/info'),
                ),
            ),
        );
    }

    public function index(Request $request)
    {
        //筛选条件
        $where = array();

        $id = $request->input('id');
        $status = $request->input('status');

        if ($id) {
            $where['id'] = $id;
        }
        if ($status) {
            $where['status'] = $status;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['id'] = $id;
        $pageMaps['status'] = $status;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/banner/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $bannerModel = new bannerModel;
        $list = $bannerModel->loadList($where, $pageAry);
        View::assign('list', $list);
        View::assign('_page', $list->render());
        View::assign('pageMaps', $pageMaps);

        return view('banner/index');
    }

    public function info(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $bannerModel = new bannerModel;
        if ($method == 'POST') {
            if ($id) {
                $status = $bannerModel->editAll($request);
            } else {
                $status = $bannerModel->addall($request);
            }
            if ($status !== false) {
                return return_json(1, '操作成功', url('banner/index'));
            } else {
                return return_json(0, '操作失败');
            }
        } else {
            $info = $bannerModel->getInfo($id);
            View::assign('info', $info);

            return view('banner/info');
        }
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $bannerModel = new bannerModel;
        if ($bannerModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
