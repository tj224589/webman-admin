<?php
namespace app\controller;

use support\Request;
use think\facade\Db;

class Ajax extends Base
{
    /**
     * 更新字段 主库
     */
    public function upField(Request $request){
        $table = $request->input('table');				//表名
        $id_name = $request->input('id_name');			//条件字段
        $id_value = $request->input('id_value');		//条件值
        $field = $request->input('field');				//修改的字段
        $field_value = $request->input('field_value');	//修改的值
        
        if ($table==null || $id_name==null || $id_value==null || $field==null || $field_value===false){
            return return_json(0,'参数不足');
        }
		
		$table = strtolower($table);
        $where[$id_name] = $id_value;
        $status = Db::name($table)->where($where)->update([$field => $field_value]);
        if ($status){
            return return_json(1,'操作成功');
        }else{
            return return_json(0,'操作失败');
        }
    }
	
}
