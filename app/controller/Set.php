<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\Config as configModel;
use app\model\ConfigUpload as uploadModel;

class Set extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '系统设置',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '站点信息',
                    'url' => url('set/site'),
                    'icon' => 'list',
                ),
                array(
                    'name' => '模板设置',
                    'url' => url('set/tpl'),
                    'icon' => 'eye',
                ),
                array(
                    'name' => '手机设置',
                    'url' => url('set/mobile'),
                    'icon' => 'mobile',
                ),
                array(
                    'name' => '上传设置',
                    'url' => url('set/upload'),
                    'icon' => 'upload',
                ),
                array(
                    'name' => '短信配置',
                    'url' => url('set/sms'),
                    'icon' => 'sms',
                ),
            ),
            '_info' => array(),
        );
    }

    /**
     * 站点设置
     */
    public function site(Request $request)
    {
        $method = $request->method();
        $configModel = new configModel;
        if ($method == 'POST') {
            if ($configModel->editAll($request)) {
                return return_json(1, '配置成功！');
            } else {
                return return_json(0, '配置失败！');
            }
        } else {
            $info = $configModel->getInfo();
            View::assign('info', $info);

            return view('set/site');
        }
    }

    /**
     * 模板设置
     */
    public function tpl(Request $request)
    {
        $method = $request->method();
        $configModel = new configModel;
        if ($method == 'POST') {
            if ($configModel->editAll($request)) {
                return return_json(1, '配置成功！');
            } else {
                return return_json(0, '配置失败！');
            }
        } else {
            $info = $configModel->getInfo();
            View::assign('info', $info);

            return view('set/tpl');
        }
    }

    /**
     * 手机设置
     */
    public function mobile(Request $request)
    {
        $method = $request->method();
        $configModel = new configModel;
        if ($method == 'POST') {
            if ($configModel->editAll($request)) {
                return return_json(1, '配置成功！');
            } else {
                return return_json(0, '配置失败！');
            }
        } else {
            $info = $configModel->getInfo();
            View::assign('info', $info);

            return view('set/mobile');
        }
    }

    /**
     * 短信配置
     */
    public function sms(Request $request)
    {
        $method = $request->method();
        $configModel = new configModel;
        if ($method == 'POST') {
            if ($configModel->editAll($request)) {
                return return_json(1, '配置成功！');
            } else {
                return return_json(0, '配置失败！');
            }
        } else {
            $info = $configModel->getInfo();
            View::assign('info', $info);

            return view('set/sms');
        }
    }

    /**
     * 上传设置
     */
    public function upload(Request $request)
    {
        $method = $request->method();
        $uploadModel = new uploadModel;
        if ($method == 'POST') {
            if ($uploadModel->editAll($request)) {
                return return_json(1, '上传设置成功！');
            } else {
                return return_json(0, '上传设置失败！');
            }
        } else {
            $info = $uploadModel->getInfo(['id'=>1]);
            View::assign('info', $info);

            return view('set/upload');
        }
    }

}
