<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\User as userModel;
use app\extend\Random;

class User extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '会员管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('user/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('user/info'),
                ),
            ),
        );
    }

    public function index(Request $request)
    {
        //筛选条件
        $where = array();

        $id = $request->input('id');
        $username = $request->input('username');
        $status = $request->input('status');

        if ($id) {
            $where['id'] = $id;
        }
        if ($username) {
            $where['username'] = $username;
        }
        if ($status) {
            $where['status'] = $status;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['id'] = $id;
        $pageMaps['username'] = $username;
        $pageMaps['status'] = $status;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/user/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $userModel = new userModel;
        $list = $userModel->loadList($where, $pageAry);
        View::assign('list', $list);
        View::assign('_page', $list->render());
        View::assign('pageMaps', $pageMaps);

        return view('user/index');
    }

    public function info(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $userModel = new userModel;
        if ($method == 'POST') {
            if ($id) {
                $status = $userModel->editAll($request);
            } else {
                $status = $userModel->addall($request);
            }
            if ($status !== false) {
                return return_json(1, '操作成功', url('user/index'));
            } else {
                return return_json(0, '操作失败');
            }
        } else {
            $info = $userModel->getInfo($id);
            View::assign('info', $info);

            return view('user/info');
        }
    }

    public function password(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $userModel = new userModel;
        if ($method == 'POST') {
            $status = false;
            $tokenTime = date('Ymd') . ceil(date('H')/2);

            $password = $request->input('password');
            $twopassword = $request->input('twopassword');
            $token = $request->input('token');

            if($password != $twopassword){
                return return_json(0,'两次密码输入不一致');
            }
            if(strlen($password) < 6 || strlen($password) > 16){
                return return_json(0,'密码长度太短或太长');
            }
            if($token != $tokenTime){
                return return_json(0,'口令错误');
            }

            if ($id){
                $Random = new Random();
                $salt = strtolower($Random::strnum(22));
                $password = md5(md5($password) . $salt . $id);

                $temp['id'] = $id;
                $temp['password'] = $password;
                $temp['salt'] = $salt;
                $status = $userModel->saved($temp);
            }
            if($status!==false){
                return return_json(1,'操作成功',url('user/index'));
            }else{
                return return_json(0,'操作失败');
            }
        }else{
            View::assign('id',$id);

            return view('user/password');
        }
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $userModel = new userModel;
        if ($userModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
