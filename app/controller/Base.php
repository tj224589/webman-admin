<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\Admin;
use app\model\Config;

class Base
{
    public function beforeAction(Request $request)
    {
        //赋值当前菜单
        if (method_exists($this, '_infoModule')) {
            View::assign('infoModule', $this->_infoModule());
        }

        //验证系统账户唯一登录
        $session = $request->session();
        $controller = $request->controller;
        if($controller!='app\controller\Index'){
            $infoMember = $this->checkToken($session);
            if (empty($infoMember)) {
                $session->forget(['id', 'username', 'nickname', 'token', 'roleId', 'sorttime']);
                return redirect('/index/index');
            }
            $this->assign('infoMember', $infoMember);
        }

        //获取系统配置
        $this->getConfig();
    }

    public function afterAction(Request $request, $response)
    {

    }

    protected function checkToken($session)
    {
        $this->assign('admin_id', $session->get('id'));

        $modelAdmin = new Admin;
        $where['id'] = $session->get('id');
        $where['token'] = $session->get('token');
        $infoMember = $modelAdmin->where($where)->find();
        return $infoMember;
    }

    protected function fetch($view)
    {
        return view($view);
    }

    protected function assign($param, $data = [])
    {
        View::assign($param, $data);
    }

    public function getConfig()
    {
        $modelConfig = new Config;
        $infoConfig = $modelConfig->getInfo();
        $this->assign('infoConfig', $infoConfig);
    }

}
