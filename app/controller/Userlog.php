<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\User as userModel;
use app\model\UserLog as userLogModel;

class Userlog extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '会员日志管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('userlog/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(

            ),
        );
    }

    public function index(Request $request)
    {
        $userModel = new userModel;
        $userLogModel = new userLogModel;

        //筛选条件
        $where = array();
        $username = $request->input('username');
        $type = $request->input('type');

        if ($username) {
            $mapUser['username'] = $username;
            $where['uid'] = $userModel->where($mapUser)->value('id');
        }
        if ($type) {
            $where['type'] = $type;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['username'] = $username;
        $pageMaps['type'] = $type;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/userlog/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $list = $userLogModel->loadList($where, $pageAry);
        if (!empty($list)){
            foreach ($list as $key => $item){
                $item['username'] = $userModel->where('id',$item['id'])->value('username');
            }
        }
        View::assign('list', $list);
        View::assign('_page', $list->render());
        View::assign('pageMaps', $pageMaps);

        return view('userlog/index');
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $userLogModel = new userLogModel;
        if ($userLogModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
