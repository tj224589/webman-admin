<?php
namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\AdminMenu;
use app\model\Config;
use app\model\Guide as guideModel;

class Home extends Base
{
    public function index(Request $request)
    {
        //顶部横向菜单
		$modelAdminMenu = new AdminMenu;
		$where['topid'] = 0;
		$where['status'] = 1;
		$list = $modelAdminMenu->getList($where);
		View::assign('list', $list);

        //左侧纵向菜单
		$listSub = $modelAdminMenu->getPurview();
		View::assign('listSub', $listSub);

        //登录信息
		$session = $request->session();
		$loginUserInfo = $session->all();
		View::assign('loginUserInfo', $loginUserInfo);

        //版本信息
		View::assign('version', 1);

        //引导
        $guideModel = new guideModel;
        $whereGuide['status'] = 1;
        $listGuide = $guideModel->getList($whereGuide,'sort asc');
        View::assign('listGuide', $listGuide);
		
        return view('home/index');
    }
	
	//控制台
	public function main(Request $request){
		$modelConfig = new Config;
        $infoConfig = $modelConfig->getInfo();
		View::assign('config', $infoConfig);

        //登录信息
        $session = $request->session();
        $loginUserInfo = $session->all();
        View::assign('loginUserInfo', $loginUserInfo);

		return view('home/main');
	}

}
