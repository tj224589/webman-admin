<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\Guide as guideModel;

class Guide extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '引导管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('guide/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('guide/info'),
                ),
            ),
        );
    }

    public function index(Request $request)
    {
        //筛选条件
        $where = array();

        $id = $request->input('id');
        $status = $request->input('status');

        if ($id) {
            $where['id'] = $id;
        }
        if ($status) {
            $where['status'] = $status;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['id'] = $id;
        $pageMaps['status'] = $status;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/page/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $guideModel = new guideModel;
        $list = $guideModel->loadList($where, $pageAry);
        View::assign('list', $list);
        View::assign('_page', $list->render());
        View::assign('pageMaps', $pageMaps);

        return view('guide/index');
    }

    public function info(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $guideModel = new guideModel;
        if ($method == 'POST') {
            if ($id) {
                $status = $guideModel->editAll($request);
            } else {
                $status = $guideModel->addall($request);
            }
            if ($status !== false) {
                return return_json(1, '操作成功', url('guide/index'));
            } else {
                return return_json(0, '操作失败');
            }
        } else {
            $info = $guideModel->getInfo($id);
            View::assign('info', $info);

            return view('guide/info');
        }
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $guideModel = new guideModel;
        if ($guideModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
