<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\model\Log as LogModel;
use app\model\Admin as adminModel;

class Log extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '日志管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('log/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(

            ),
        );
    }

    public function index(Request $request)
    {
        $logModel = new LogModel;
        $adminModel = new adminModel;

        //筛选条件
        $where = array();
        $username = $request->input('username');
        $type = $request->input('type');

        if ($username) {
            $mapAdmin['username'] = $username;
            $where['aid'] = $adminModel->where($mapAdmin)->value('id');
        }
        if ($type) {
            $where['type'] = $type;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['username'] = $username;
        $pageMaps['type'] = $type;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/log/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $list = $logModel->loadList($where, $pageAry);
        if (!empty($list)){
            foreach ($list as $key => $item){
                $item['username'] = $adminModel->where('id',$item['id'])->value('username');
            }
        }
        View::assign('list', $list);
        View::assign('_page', $list->render());
        View::assign('pageMaps', $pageMaps);

        return view('log/index');
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $logModel = new LogModel;
        if ($logModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
