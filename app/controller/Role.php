<?php

namespace app\controller;

use app\model\User as userModel;
use support\Request;
use support\View;
use think\facade\Db;
use app\model\Admin as adminModel;
use app\model\AdminRole as roleModel;
use app\model\AdminMenu;

class Role extends Base
{
    /**
     * 当前模块参数
     */
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '用户组管理',
                'description' => '管理网站后台用户组',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('role/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(
                array(
                    'name' => '添加',
                    'url' => url('role/info'),
                ),
            ),
        );
    }

    /**
     * 列表
     */
    public function index(Request $request)
    {
        $roleModel = new roleModel;
        $adminModel = new adminModel;

        $status = $request->input('status');

		$where = array();
        if($status){
            $where['status'] = $status;
        }

		$pageMaps = array();
        $pageMaps['status'] = $status;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/admin/index',
            'query' => $pageMaps,
        ];

        $list = $roleModel->loadList($where,$pageAry);
        if (!empty($list)) {
            foreach ($list as $key => $item) {
                $map['roleId'] = $item['id'];
                $list[$key]['userCount'] = $adminModel->countNum($map);
            }
        }
        $this->assign('list', $list);
        $this->assign('_page',$list->render());
        $this->assign('pageMaps',$pageMaps);

        return $this->fetch('role/index');
    }

    /**
     * 详情
     */
    public function info(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $roleModel = new roleModel;
        if ($method == 'POST') {
            if ($id == 1) {
                return return_json(0, '保留用户组无法编辑！');
            }
            if ($id) {
                $status = $roleModel->editAll($request);
            } else {
                $status = $roleModel->addAll($request);
            }
            if ($status !== false) {
                return return_json(1, '操作成功', url('role/index'));
            } else {
                return return_json(0, '操作失败');
            }
        } else {
            $this->assign('info', $roleModel->getInfo($id));
			
            return $this->fetch('role/info');
        }
    }

    /**
     * 权限
     */
    public function purview(Request $request)
    {
        $method = $request->method();
        $id = $request->input('id');
        $roleModel = new roleModel;
        $menuModel = new AdminMenu;
        if ($method == 'POST') {
            if (empty($id)) {
                return return_json(0, '参数不能为空！');
            }
            if ($roleModel->savePurviewData($request)) {
                return return_json(1, '权限更新成功！', url('role/index'));
            } else {
                return return_json(0, '权限更新失败！');
            }
        } else {
            $info = $roleModel->getInfo($id);
			$info['base_purview'] = str_explode(',',$info['base_purview']);
			$info['menu_purview'] = str_explode(',',$info['menu_purview']);
			$this->assign('info', $info);
			
            $AdminMenu = $menuModel->getPurview();//后台菜单
            $this->assign('AdminMenu', $AdminMenu);

            return $this->fetch('role/purview');
        }
    }


    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空！');
        }
        if ($id == 1) {
            return return_json(0, '保留用户组无法删除');
        }

        $roleModel = new roleModel;
        $adminModel = new adminModel;

        //获取用户数量
        $map = array();
        $map['roleId'] = $id;
        $countUser = $adminModel->countNum($map);
        if ($countUser > 0) {
            return return_json(0, '请先删除该组下的用户！');
        }

        if ($roleModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败！');
        }
    }


}

