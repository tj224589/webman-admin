<?php

namespace app\controller;

use support\Request;
use app\model\Visit as VisitModel;

class Visit extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '来访记录',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('visit/index'),
                    'icon' => 'list',
                ),
            ),
            '_info' => array(

            ),
        );
    }

    public function index(Request $request)
    {
        $visitModel = new VisitModel;

        //筛选条件
        $where = array();
        $ip = $request->input('ip');

        if ($ip) {
            $where['ip'] = $ip;
        }

        //URL参数
        $pageMaps = array();
        $pageMaps['ip'] = $ip;

        $pageAry = [
            'list_rows' => 10,
            'page' => $request->input('page', 1),
            'path' => '/visit/index',
            'query' => $pageMaps,
        ];

        //查询数据
        $list = $visitModel->loadList($where, $pageAry);
        $this->assign('list', $list);
        $this->assign('_page', $list->render());
        $this->assign('pageMaps', $pageMaps);

        return view('visit/index');
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) {
            return return_json(0, '参数不能为空');
        }

        $visitModel = new VisitModel;
        if ($visitModel->del($id)) {
            return return_json(1, '删除成功！');
        } else {
            return return_json(0, '删除失败');
        }
    }

}
