<?php

namespace app\controller;

use support\Request;
use support\View;
use think\facade\Db;
use app\extend\Backupandrestore;

class Backup extends Base
{
    protected function _infoModule()
    {
        return array(
            'info' => array(
                'name' => '备份管理',
                'description' => '管理网站后台管理员',
            ),
            'menu' => array(
                array(
                    'name' => '列表',
                    'url' => url('backup/index'),
                    'icon' => 'list',
                ),
                array(
                    'name' => '数据表',
                    'url' => url('backup/mysql'),
                    'icon' => 'eye',
                ),
            ),
            '_info' => array(

            ),
        );
    }

    private function configDB(){
        $mysqlConfig = config('thinkorm.connections.mysql');
        $mysqlConfig['path'] = public_path() . "/mysql/";		//存储路径
        return $mysqlConfig;
    }

    public function index(Request $request)
    {
        $config = $this->configDB();

        $mysqlUrl = $config['path'];
        $FileArr = myScandir($mysqlUrl);
        $mysqlList = array();
        foreach ($FileArr as $i => $n){
            if($n != 'PHPMyAdminInitialData.sql' && $i>1){

                $FileSize = filesize($mysqlUrl . $n)/1024;
                if ($FileSize < 1024){
                    $FileSize = number_format($FileSize,2) . ' KB';
                } else {
                    $FileSize = '<font color="#FF0000">' . number_format($FileSize/1024,2) . '</font> MB';
                }

                $mysqlList[$i-2]['size'] = $FileSize;
                $mysqlList[$i-2]['time'] = date('Y-m-d H:i:s',filemtime($mysqlUrl.$n));
                $mysqlList[$i-2]['file'] = $n;
            }
        }
        $this->assign('list',$mysqlList);
        $this->assign('dbname',$config['database']);
        $this->assign('dbprefix',$config['prefix']);

        return view('backup/index');
    }

    //备份数据库
    public function back(Request $request)
    {
        $config = $this->configDB();

        $backup = new Backupandrestore($config);
        $info = $backup->backup();

        return return_json(1, $info);
    }

    //下载
    public function download(Request $request)
    {
        $file = $request->input('file');
        $config = $this->configDB();
        return response()->download($config['path'] . $file , $file);
    }

    //还原
    public function restore(Request $request)
    {
        $file = $request->input('file');

        $config = $this->configDB();
        $backup = new Backupandrestore($config);
        $info = $backup->restore($file);

        return return_json(1, $info);
    }

    //删除
    public function del(Request $request)
    {
        $file = $request->input('file');

        $config = $this->configDB();
        $backup = new Backupandrestore($config);
        $info = $backup->delfilename($file);

        return return_json(1, $info);
    }

    //表列表
    public function mysql(Request $request)
    {
        $config = $this->configDB();
        $backup = new Backupandrestore($config);
        $list = $backup->get_tablename();
        $this->assign('list',$list);

        return view('backup/mysql');
    }

    //表结构
    public function structure(Request $request)
    {
        $tablename = $request->input('tablename');

        $config = $this->configDB();
        $backup = new Backupandrestore($config);
        $info = $backup->get_table_structure($tablename);
        $this->assign('info',$info);

        return view('backup/structure');
    }

}
