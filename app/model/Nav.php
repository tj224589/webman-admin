<?php

namespace app\model;

use think\Model;

class Nav extends Model
{
    protected $name = 'nav';

    protected $pk = 'id';

    /**
     * 获取列表
     * @return array 分页列表
     */
    public function loadList($where = [], $pageAry = [], $order = 'sort asc,id asc')
    {
        $data = $this->where($where)
            ->order($order)
            ->paginate($pageAry)
            ->each(function ($item, $key) {
                $map['fid'] = $item["id"];
                $list = $this->where($map)->select();
                foreach ($list as $k => $vo) {
                    $voval['fid'] = $vo['id'];
                    $list[$k]['voval'] = $this->where($voval)->select();
                }
                $item['voo'] = $list;
                return $item;
            });
        return $data;
    }

    /**
     * 获取列表
     * @return array 列表
     */
    public function getListSub($where = array(), $order = 'sort asc,id asc')
    {
        $data = $this->where($where)->order($order)->select();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $map['fid'] = $value["id"];
                $list = $this->where($map)->order($order)->select();
                if (!empty($list)) {
                    foreach ($list as $k => $vo) {
                        $voval['fid'] = $vo['id'];
                        $list[$k]['voval'] = $this->where($voval)->order($order)->select();
                    }
                }
                $data[$key]['voo'] = $list;
            }
            $data = $data->toArray();
        }
        return $data;
    }

    /**
     * 获取列表
     * @return array 列表
     */
    public function getList($where = array(), $order = 'sort asc,id desc')
    {
        $data = $this->where($where)->order($order)->select();
        return $data;
    }

    /**
     * 获取信息
     * @param int $ID
     * @return array 信息
     */
    public function getInfo($id)
    {
        $map = array();
        $map['id'] = $id;
        return $this->getWhereInfo($map);
    }

    /**
     * 获取信息
     * @param array $where 条件
     * @return array 信息
     */
    public function getWhereInfo($where)
    {
        $info = $this->where($where)->find();
        return $info;
    }

    /**
     * 获取字段值
     * @param array $where 条件
     * @param array $field 字段
     * @return field 信息
     */
    public function getWhereValue($where, $field)
    {
        $data = $this->where($where)->value($field);
        return $data;
    }

    /**
     * 获取列的值
     * @param array $where 条件
     * @param array $field 字段
     * @return array 信息
     */
    public function getWhereColumn($where, $field)
    {
        $data = $this->where($where)->column($field);
        return $data;
    }

    /**
     * 新增
     */
    public function addall($request)
    {
        $data = $request->all();
        return $this->save($data);
    }

    /**
     * 更新
     */
    public function editAll($request)
    {
        if (empty($request->input('id')) || $request->input('id') == null) {
            return false;
        }

        $data = $request->all();
        return $this->saved($data);
    }

    //数据新增
    public function add($data)
    {
        //启动事务
        $this->startTrans();
        try {
            $insertid = self::insertGetId($data);
            //提交事务
            $this->commit();
            return $insertid;
        } catch (\Exception $e) {
            //回滚事务
            $this->rollback();
        }
    }

    //数据更新
    public function saved($data)
    {
        $this->update($data);
        return true;
    }

    /**
     * 删除信息
     * @param int $id ID
     * @return bool 删除状态
     */
    public function del($id)
    {
        $map = array();
        $map['id'] = $id;
        return $this->where($map)->delete();
    }

}
