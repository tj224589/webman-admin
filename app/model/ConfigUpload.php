<?php

namespace app\model;

use think\Model;

class ConfigUpload extends Model
{
    /**
     * 获取信息
     * @return array 网站配置
     */
    public function getInfo($where = array())
    {
        $data = $this->where($where)->find();
        return $data;
    }

    /**
     * 更新
     */
    public function editAll($request)
    {
        $data = $request->all();
        if (empty($data)) {
            return false;
        }

        return $this->update($data);
    }


}
