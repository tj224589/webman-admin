<?php
namespace app\model;

use think\Model;
use app\extend\Category;

class AdminMenu extends Model
{
    protected $name  = 'admin_menu';

    protected $pk = 'id';
	
    /**
     * 菜单数据
     */
    public function loadList($where = array(), $id=0){
        $data = $this->loadData($where);
        $cat = new Category(array('id', 'topid', 'name', 'cname'));
        $data = $cat->getTree($data, intval($id));
        return $data;
    }
	/**
     * 菜单数据
     */
    public function loadData($where = array(), $limit = 0,$order='sort ASC,id ASC'){
        $list = $this->where($where)->order($order)->limit($limit)->select();
		if(!empty($list)){
			$list = $list->toArray();
		}
        return $list;
    }
	
	public function getList($where = array(),$order='sort ASC,id ASC'){
        $list = $this->where($where)->order($order)->select();
		if(!empty($list)){
			$list = $list->toArray();
		}
		return $list;
    }
	
    public function getPurview(){
		$where['topid'] = 0;
		$where['status'] = 1;
        $list = $this->where($where)->order('sort ASC,id ASC')->select();
		if(!empty($list)){
			$list = $list->toArray();
			foreach($list as $key=>$val){
                if ($val['act']!=null){
                    $list[$key]['act'] = json_decode($val['act'],true);
                }

				$map['topid'] = $val['id'];
				$map['status'] = 1;
				$list_sub = $this->getList($map);
				if(!empty($list_sub)){
					foreach($list_sub as $k=>$vo){
                        if ($vo['act']!=null){
                            $list_sub[$k]['act'] = json_decode($vo['act'],true);
                        }

						$mapVoo['topid'] = $vo['id'];
						$mapVoo['status'] = 1;
						$sub_voo = $this->getList($mapVoo);
                        if (!empty($sub_voo)){
                            foreach ($sub_voo as $kk=>$vv){
                                if ($vv['act']!=null){
                                    $sub_voo[$kk]['act']=json_decode($vv['act'],true);
                                }
                            }
                        }
						$list_sub[$k]['sub_voo'] = $sub_voo;
					}
				}
				$list[$key]['list_sub'] = $list_sub;
			}
		}
        return $list;
    }
	
    /**
     * 获取信息
     * @param int $classId ID
     * @return array 信息
     */
    public function getInfo($id){
        $map = array();
        $map['id'] = $id;
        return $this->getWhereInfo($map);
    }
    /**
     * 获取信息
     * @param array $where 条件
     * @return array 信息
     */
    public function getWhereInfo($where){
        $info = $this->where($where)->find();
		if (!empty($info['act'])){
            $info['act'] = json_decode($info['act'],true);
        }
        return $info;
    }
	
	/**
     * 获取字段值
     * @param array $where 条件
     * @param array $field 字段
     * @return field 信息
     */
    public function getWhereValue($where,$field)
    {
        $data = $this->where($where)->value($field);
        return $data;
    }
	
	/**
     * 获取列的值
     * @param array $where 条件
     * @param array $field 字段
     * @return array 信息
     */
    public function getWhereColumn($where,$field)
    {
        $data = $this->where($where)->column($field);
        return $data;
    }

    /**
     * 新增
     */
    public function addall($request){
		$data = $request->all();		
		if(!empty($data['act'])){
			$data['act'] = json_encode($data['act']);
		}
		
        return $this->save($data);
    }
	
    /**
     * 更新
     */
    public function editAll($request){
        if (empty($request->input('id')) || $request->input('id')==null){
            return false;
        }
		
		$data = $request->all();
		if(!empty($data['act'])){
			$data['act'] = json_encode($data['act']);
		}
		
        return $this->saved($data);
    }
	
	//数据新增
    public function add($data)
    {
        //启动事务
        $this->startTrans();
        try {
            $insertid = self::insertGetId($data);
            //提交事务
            $this->commit();
            return $insertid;
        } catch (\Exception $e) {
            //回滚事务
            $this->rollback();
        }
    }
	
	//数据更新
	public function saved($data){
		$this->update($data);
		return true;
	}
	
    /**
     * 删除数据
     * @param 栏目id $class_id
     * @return 1|0
     */
    public function del($id){
        $map = array();
        $map['id'] = $id;
        return $this->where($map)->delete();
    }
}