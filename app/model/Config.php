<?php

namespace app\model;

use think\Model;

class Config extends Model
{
    /**
     * The name associated with the model.
     *
     * @var string
     */
    protected $name = 'config';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 获取信息
     * @return array 网站配置
     */
    public function getInfo($where = array(), $field = '*')
    {
        $list = $this->field($field)->where($where)->select();
        $config = array();
        foreach ($list as $key => $value) {
            $config[$value['name']] = $value['data'];
            $config['note'.$key] = $value['note'];
        }
        return $config;
    }

    /**
     * 更新
     */
    public function editAll($request)
    {
        $data = $request->all();
        if (empty($data)) {
            return false;
        }

        foreach ($data as $name => $value) {
            $map = array('name' => $name);
            $status = $this->where($map)->update(['data' => $value]);
            if ($status === false) {
                return false;
            }
        }
        return true;
    }

}