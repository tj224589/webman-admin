<?php

namespace app\model;

use think\facade\Db;
use think\Model;

class Article extends Model
{
    protected $name = 'article';

    protected $pk = 'id';

    /**
     * 获取列表
     * @return array 分页列表
     */
    public function loadList($where = [], $pageAry = [], $order = 'id desc')
    {
        $data = $this->where($where)->order($order)->paginate($pageAry);
        return $data;
    }

    /**
     * 获取列表
     * @return array 列表
     */
    public function getList($where = array(), $order = 'id desc')
    {
        $data = $this->where($where)->order($order)->select();
        return $data;
    }

    /**
     * 获取信息
     * @param int $ID
     * @return array 信息
     */
    public function getInfo($id)
    {
        $map = array();
        $map['id'] = $id;
        return $this->getWhereInfo($map);
    }

    /**
     * 获取信息
     * @param array $where 条件
     * @return array 信息
     */
    public function getWhereInfo($where)
    {
        $info = $this->where($where)->find();
        if (!empty($info)) {
            $info['content'] = '';
            $infoContent = Db::name('article_content')->where('article_id', $info['id'])->find();
            if (!empty($infoContent)) {
                $info['content'] = $infoContent['content'];
            }
        }
        return $info;
    }

    /**
     * 获取字段值
     * @param array $where 条件
     * @param array $field 字段
     * @return field 信息
     */
    public function getWhereValue($where, $field)
    {
        $data = $this->where($where)->value($field);
        return $data;
    }

    /**
     * 获取列的值
     * @param array $where 条件
     * @param array $field 字段
     * @return array 信息
     */
    public function getWhereColumn($where, $field)
    {
        $data = $this->where($where)->column($field);
        return $data;
    }

    /**
     * 新增
     */
    public function addall($request)
    {
        $data = $request->all();

        if (!empty($data['content']) || $data['content'] != '') {
            $map['content'] = $data['content'];
            unset($data['content']);
        }
        $data['addtime'] = time();
        $this->save($data);

        $map['article_id'] = $this->id;
        Db::name('article_content')->insert($map);

        return $this->id;
    }

    /**
     * 更新
     */
    public function editAll($request)
    {
        if (empty($request->input('id')) || $request->input('id') == null) {
            return false;
        }

        $data = $request->all();

        if (!empty($data['content']) || $data['content'] != '') {
            $map['content'] = $data['content'];
            $map['article_id'] = $data['id'];
            unset($data['content']);

            $infoContent = Db::name('article_content')->where('article_id', $data['id'])->find();
            if (!empty($infoContent)) {
                $map['id'] = $infoContent['id'];
            }
            Db::name('article_content')->save($map);
        }

        $data['addtime'] = time();
        return $this->saved($data);
    }

    //数据新增
    public function add($data)
    {
        //启动事务
        $this->startTrans();
        try {
            $insertid = self::insertGetId($data);
            //提交事务
            $this->commit();
            return $insertid;
        } catch (\Exception $e) {
            //回滚事务
            $this->rollback();
        }
    }

    //数据更新
    public function saved($data)
    {
        $this->update($data);
        return true;
    }

    /**
     * 删除信息
     * @param int $id ID
     * @return bool 删除状态
     */
    public function del($id)
    {
        $map = array();
        $map['id'] = $id;

        Db::name('article_content')->where('article_id', $id)->delete();

        return $this->where($map)->delete();
    }

}
