<?php

namespace app\model;

use think\Model;

class AdminRole extends Model
{
    protected $name = 'admin_role';

    protected $pk = 'id';

    /**
     * 获取列表
     * @return array 列表
     */
    public function loadList($where = [], $pageAry = [], $order = 'id asc')
    {
        $data = $this->where($where)->order($order)->paginate($pageAry);
        return $data;
    }

    /**
     * 获取列表
     * @return array 列表
     */
    public function getList($where = array(), $order = 'id asc')
    {
        $list = $this->where($where)->order($order)->select();
        if (!empty($list)) {
            $list = $list->toArray();
        }
        return $list;
    }

    /**
     * 获取信息
     * @param int $ID
     * @return array 信息
     */
    public function getInfo($id = 1)
    {
        $map = array();
        $map['id'] = $id;
        return $this->getWhereInfo($map);
    }

    /**
     * 获取信息
     * @param array $where 条件
     * @return array 信息
     */
    public function getWhereInfo($where)
    {
        $info = $this->where($where)->find();
        return $info;
    }

    /**
     * 获取字段值
     * @param array $where 条件
     * @param array $field 字段
     * @return field 信息
     */
    public function getWhereValue($where, $field)
    {
        $data = $this->where($where)->value($field);
        return $data;
    }

    /**
     * 获取列的值
     * @param array $where 条件
     * @param array $field 字段
     * @return array 信息
     */
    public function getWhereColumn($where, $field)
    {
        $data = $this->where($where)->column($field);
        return $data;
    }

    /**
     * 新增
     */
    public function addAll($request)
    {
        $data = $request->all();

        return $this->add($data);
    }

    /**
     * 更新
     */
    public function editAll($request)
    {
        if (empty($request->input('id')) || $request->input('id') == null) {
            return false;
        }

        $data = $request->all();
        return $this->saved($data);
    }

    //数据新增
    public function add($data)
    {
        //启动事务
        $this->startTrans();
        try {
            $insertid = self::insertGetId($data);
            //提交事务
            $this->commit();
            return $insertid;
        } catch (\Exception $e) {
            //回滚事务
            $this->rollback();
        }
    }

    //数据更新
    public function saved($data)
    {
        $this->update($data);
        return true;
    }

    /**
     * 更新权限
     * @param string $type 更新类型
     * @return bool 更新状态
     */
    public function savePurviewData($request)
    {
        $data = array();
        $data['id'] = $request->input('id');

        $menu_purview = $request->input('menu_purview');
        $base_purview = $request->input('base_purview');

        if (!empty($menu_purview)){
            $data['menu_purview'] = implode(',',$menu_purview);
        }
        if (!empty($base_purview)){
            $data['base_purview'] = implode(',',$base_purview);
        }

        $status = $this->saved($data);
        if ($status === false) {
            return false;
        }
        return true;
    }

    /**
     * 删除信息
     * @param int $id
     * @return bool 删除状态
     */
    public function del($id)
    {
        $map = array();
        $map['id'] = $id;
        return $this->where($map)->delete();
    }

}
