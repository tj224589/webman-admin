# webman - admin
基于 [webman](https://gitee.com/walkor/webman.git) 1.3.20 + ThinkORM 2.0.53 + ThinkTemplate 2.0.8 打造的一套 php（>=7.2）通用后台内容管理系统。

# 下载
https://gitee.com/tj224589/webman-admin.git

# 文档
https://www.workerman.net/doc/webman/view.html

# 说明
数据库sql文件备份在public/mysql中，请自行导入mysql

数据库连接配置在config/thinkorm.php，请自行修改

# 访问
在浏览器里访问 http://127.0.0.1:8787

如需绑定域名 请使用nginx反向代理

# 截图
![image](public/uploads/20220723141222.png)

![image](public/uploads/20220723141249.png)

![image](public/uploads/20220723141356.png)

![image](public/uploads/20220723141420.png)